#!/bin/bash

while true; do
	xsetroot -name "$(date "+%D, %T")"
	sleep 1
done

# https://man7.org/linux/man-pages/man1/date.1.html
